#! /bin/bash
set -e

SOLOv2_ROOT=$WRKSPCE/SOLOv2 && \
git clone /vol/research/content4all/3dhumanpose/projects/SOLOv2 $SOLOv2_ROOT && \
cd $SOLOv2_ROOT && \
exec python run_video.py \
    --src_video $1 \
    --dst_video $2 \
    --cfg $CFG_SOLOv2_X101_DCN_3x \
    --ckpt $CKPT_SOLOv2_X101_DCN_3x
