#! /bin/bash
# This is for a local install.
conda create -y -n solo-env python=3.7 pip
conda activate solo-env
conda install -y pytorch=1.3.0 torchvision=0.4.1 cudatoolkit=10.1 -c pytorch

git clone https://github.com/WXinlong/SOLO.git SOLO
cd SOLO
git checkout c7b294a311bfbc59b982b29dc9d12eff42ca0acb
pip install cython pycocotools tqdm scikit-video
pip install -e .
mkdir checkpoints
wget https://cloudstor.aarnet.edu.au/plus/s/KV9PevGeV8r4Tzj/download -O checkpoints/SOLOv2_X101_DCN_3x.pth
cd ..

